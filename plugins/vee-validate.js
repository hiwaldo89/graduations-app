import { Validator } from 'vee-validate'
const dict = {
  custom: {
    quantity: {
      max_value: 'El monto no puede ser mayor a la deuda'
    }
  }
}

Validator.localize('es', dict)
