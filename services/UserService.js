import https from 'https'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  getGraduates(token) {
    return apiClient({
      method: 'get',
      url: '/wp/v2/users?roles=graduate',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  getGraduate(id, token) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/users/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  getGraduatesByEventId(id, token, page) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/users?per_page=20&page=${page}&filter[meta_key]=event&filter[meta_value]=${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  updateGraduate(id, data, token) {
    return apiClient({
      method: 'patch',
      url: `/graduations/v1/graduates/${id}`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json;charset=UTF-8'
      }
    })
  },
  getAgents(token) {
    return apiClient({
      method: 'get',
      url: '/wp/v2/users?roles=sales_agent',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  getAgent(id, token) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/users/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  createAgent(user, token) {
    return apiClient({
      method: 'post',
      url: '/graduations/v1/agents',
      data: user,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  updateAgent(id, data, token) {
    return apiClient({
      method: 'patch',
      url: `/graduations/v1/agents/${id}`,
      data: data,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json;charset=UTF-8'
      }
    })
  },
  deleteUser(id, token) {
    return apiClient({
      method: 'delete',
      url: `/wp/v2/users/${id}?force=true&reassign=9`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  sendConfirmationEmail(id, token) {
    return apiClient({
      method: 'post',
      url: `/graduations/v1/send-confirmation/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
