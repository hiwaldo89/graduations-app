import https from 'https'
import formurlencoded from 'form-urlencoded'
import axios from 'axios'
//import { VueLoaderPlugin } from 'vue-loader'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  login(credentials) {
    return apiClient({
      method: 'post',
      url: '/jwt-auth/v1/token',
      data: formurlencoded({
        username: credentials.username,
        password: credentials.password
      })
    })
  },
  getSelfUser(token) {
    return apiClient({
      method: 'get',
      url: '/wp/v2/users/me',
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  registerGraduate(user) {
    return apiClient({
      method: 'post',
      url: '/wp/v2/users/register',
      data: {
        //username: user.username,
        first_name: user.name,
        last_name: user.lastname,
        email: user.email,
        password: user.password,
        event: user.event,
        phone: user.phone,
        tickets: user.tickets,
        special_menu: user.special_menu
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
  },
  verifyAccount(authData) {
    return apiClient({
      method: 'patch',
      url: `/graduations/v1/verify-user/${authData.user}`,
      data: {
        verification_code: authData.key
      }
    })
  }
}
