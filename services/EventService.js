import https from 'https'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  getEvents() {
    return apiClient({
      method: 'get',
      url: '/wp/v2/event'
    })
  },
  getEvent(id) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/event/${id}`
    })
  },
  createEvent(event, uploadedFile, token) {
    return apiClient({
      method: 'post',
      url: '/graduations/v1/events',
      data: {
        title: event.title,
        school: event.school,
        date: event.date,
        ticket_price: event.ticket_price,
        venue: event.venue,
        codigo_del_evento: event.code,
        description: event.description,
        facebook_url: event.fbUrl,
        layout: uploadedFile !== undefined ? uploadedFile.data.id : ''
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  updateEvent(eventId, eventData, uploadedFileId, token) {
    return apiClient({
      method: 'patch',
      url: `/graduations/v1/events/${eventId}`,
      data: {
        title: eventData.title,
        school: eventData.school,
        date: eventData.date,
        ticket_price: eventData.ticket_price,
        venue: eventData.venue,
        codigo_del_eventDatao: eventData.code,
        description: eventData.description,
        facebook_url: eventData.fbUrl,
        layout: uploadedFileId
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  deleteEvent(id, token) {
    return apiClient({
      method: 'delete',
      url: `/wp/v2/event/${id}`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
