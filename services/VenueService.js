import https from 'https'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  getVenues() {
    return apiClient({
      method: 'get',
      url: '/wp/v2/venue'
    })
  },
  getVenue(id) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/venue/${id}`
    })
  },
  createVenue(venue, gallery, token) {
    return apiClient({
      method: 'post',
      url: '/graduations/v1/venues',
      data: {
        title: venue.title,
        description: venue.description,
        gallery: gallery
      },
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  updateVenue(venueId, data, token) {
    return apiClient({
      method: 'patch',
      url: `/graduations/v1/venues/${venueId}`,
      data: data,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      }
    })
  },
  deleteVenue(id, token) {
    return apiClient({
      method: 'delete',
      url: `/wp/v2/venue/${id}?force=true`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
