import https from 'https'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  getPayments() {
    return apiClient({
      method: 'get',
      url: '/wp/v2/payment'
    })
  },
  fetchPaymentsById(id) {
    return apiClient({
      method: 'get',
      url: `/wp/v2/payment?filter[meta_key]=user_id&filter[meta_value]=${id}`
    })
  },
  createPayment(payment, token) {
    return apiClient({
      method: 'post',
      url: '/graduations/v1/payments',
      data: payment,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  deletePayment(id, token) {
    return apiClient({
      method: 'delete',
      url: `/wp/v2/payment/${id}?force=true`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
