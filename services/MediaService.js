import https from 'https'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.apiUrl,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

export default {
  uploadFile(file, token) {
    console.log('hello')
    return apiClient({
      method: 'post',
      url: '/wp/v2/media',
      data: file,
      processData: false,
      contentType: false,
      crossDomain: true,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  },
  deleteFile(id, token) {
    return apiClient({
      method: 'delete',
      url: `/wp/v2/media/${id}?force=true`,
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
  }
}
