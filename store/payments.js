import PaymentService from '@/services/PaymentService'

export const state = () => ({
  payments: []
})

export const mutations = {
  SET_PAYMENTS(state, payments) {
    state.payments = payments
  }
}

export const actions = {
  async fetchPayments({ commit }) {
    try {
      const response = await PaymentService.getPayments()
      commit('SET_PAYMENTS', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async fetchPaymentsById({ commit }, id) {
    const payments = await PaymentService.fetchPaymentsById(id)
    commit('SET_PAYMENTS', payments.data)
  },
  createPayment({ rootState }, payment) {
    PaymentService.createPayment(payment, rootState.auth.token)
  },
  deletePayment({ rootState }, id) {
    PaymentService.deletePayment(id, rootState.auth.token)
  }
  //   async fetchPayments({ commit }) {
  //     const payments = await PaymentService.fetchPayments()
  //     commit('SET_PAYMENTS', payments.data)
  //   },
  //   async fetchPaymentsById({ commit }, id) {
  //     const payments = await PaymentService.fetchPaymentsById(id)
  //     commit('SET_PAYMENTS', payments.data)
  //   },
  //   createPayment({ rootState }, payment) {
  //     axios.defaults.headers = {
  //       Authorization: `Bearer ${rootState.auth.token}`
  //     }
  //     PaymentService.createPayment(payment)
  //   }
}
