import VenueService from '@/services/VenueService'
import MediaService from '@/services/MediaService'

export const state = () => ({
  venues: [],
  venue: {}
})

export const mutations = {
  SET_VENUES(state, venues) {
    state.venues = venues
  },
  SET_VENUE(state, venue) {
    state.venue = venue
  }
}

export const actions = {
  async fetchVenues({ commit }) {
    try {
      const response = await VenueService.getVenues()
      commit('SET_VENUES', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async fetchVenue({ commit, getters }, id) {
    const venue = getters.getVenueById(id)
    if (venue) {
      commit('SET_VENUE', venue)
    } else {
      try {
        const response = await VenueService.getVenue(id)
        commit('SET_VENUE', response.data)
      } catch (e) {
        console.log(e)
      }
    }
  },
  async createVenue({ rootState }, venue) {
    const gallery = []
    for (const image of venue.gallery) {
      const formData = new FormData()
      formData.append('file', image)
      const uploadedFile = await MediaService.uploadFile(
        formData,
        rootState.auth.token
      )
      gallery.push({
        image: {
          ID: uploadedFile.data.id
        }
      })
    }
    await VenueService.createVenue(venue, gallery, rootState.auth.token)
  },
  async updateVenue({ rootState }, { oldVersion, venue }) {
    const gallery = []
    if (venue.gallery.length > 0) {
      for (const img of venue.gallery) {
        if (img.manuallyAdded) {
          gallery.push({
            image: {
              ID: img.id
            }
          })
        } else {
          const formData = new FormData()
          formData.append('file', img)
          const uploadedFile = await MediaService.uploadFile(
            formData,
            rootState.auth.token
          )
          gallery.push({
            image: {
              ID: uploadedFile.data.id
            }
          })
        }
      }
    }
    await VenueService.updateVenue(
      oldVersion.id,
      {
        title: venue.title,
        description: venue.description,
        gallery: gallery
      },
      rootState.auth.token
    )
  },
  deleteVenue({ rootState, dispatch }, venueObj) {
    if (venueObj.gallery.length > 0) {
      for (const img of venueObj.gallery) {
        MediaService.deleteFile(img.image.ID)
      }
    }
    VenueService.deleteVenue(venueObj.id, rootState.auth.token).then(() => {
      dispatch('fetchVenues')
    })
  },
  async addEventToVenue(
    { commit, rootState, state, dispatch },
    { venueId, eventId }
  ) {
    await dispatch('fetchVenue', venueId)
    let venueEvents = []
    if (state.venue.events) {
      venueEvents = state.venue.events.slice(0)
      venueEvents.push(eventId)
    } else {
      venueEvents.push(eventId)
    }

    try {
      const venueData = {
        title: state.venue.title.rendered,
        description: state.venue.description,
        gallery: state.venue.gallery,
        events: venueEvents
      }
      await VenueService.updateVenue(venueId, venueData, rootState.auth.token)
      const updatedVenue = await dispatch('fetchVenue', venueId)
      commit('SET_VENUE', updatedVenue)
    } catch (e) {
      console.log(e)
    }
  },
  async deleteEventFromVenue(
    { state, dispatch, rootState },
    { venueId, eventId }
  ) {
    await dispatch('fetchVenue', venueId)
    let venueEvents = false
    if (state.venue.events) {
      venueEvents = state.venue.events.filter(event => {
        return event !== eventId
      })
    }
    const venueData = {
      title: state.venue.title.rendered,
      description: state.venue.description,
      gallery: state.venue.gallery,
      events: venueEvents
    }
    VenueService.updateVenue(venueId, venueData, rootState.auth.token)
  }
}

export const getters = {
  getVenueById: state => id => {
    return state.venues.find(venue => venue.id === id)
  }
}
