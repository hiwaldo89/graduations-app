import Cookie from 'js-cookie'
import AuthService from '@/services/AuthService'

export const state = () => ({
  token: null,
  user: {}
})

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
  SET_USER(state, user) {
    state.user = user
  },
  CLEAR_TOKEN(state) {
    state.token = null
  }
}

export const actions = {
  async userLogin({ commit, state }, credentials) {
    try {
      const response = await AuthService.login(credentials)
      commit('SET_TOKEN', response.data.token)
      localStorage.setItem('token', response.data.token)
      Cookie.set('token', response.data.token)
    } catch (e) {
      console.log(e)
    }

    try {
      const response = await AuthService.getSelfUser(state.token)
      commit('SET_USER', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  logout({ commit }) {
    commit('CLEAR_TOKEN')
    Cookie.remove('token')
    localStorage.removeItem('token')
  },
  async registerUser(context, user) {
    await AuthService.registerGraduate(user)
  },
  async verifyAccount(context, authData) {
    try {
      let response = await AuthService.verifyAccount(authData)
      return response
    } catch (e) {
      console.log(e)
    }
  },
  async initAuth({ commit }, req) {
    let token
    if (req) {
      if (!req.headers.cookie) {
        return
      }
      const tokenCookie = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('token='))
      if (!tokenCookie) {
        return
      }
      token = tokenCookie.split('=')[1]
    } else {
      token = localStorage.getItem('token')
    }
    commit('SET_TOKEN', token)
    try {
      const response = await AuthService.getSelfUser(token)
      commit('SET_USER', response.data)
    } catch (e) {
      console.log(e)
    }
  }
}

export const getters = {
  loggedIn(state) {
    return !!state.token
  },
  isAdmin(state) {
    if (state.user.roles) {
      return !!(
        state.user.roles[0] === 'app_admin' ||
        state.user.roles[0] === 'sales_agent'
      )
    } else {
      return false
    }
  }
}
