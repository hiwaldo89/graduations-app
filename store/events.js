import EventService from '@/services/EventService'
import MediaService from '@/services/MediaService'

export const state = () => ({
  events: [],
  event: {}
})

export const mutations = {
  SET_EVENTS(state, events) {
    state.events = events
  },
  SET_EVENT(state, event) {
    state.event = event
  }
}

export const actions = {
  async fetchEvents({ commit }) {
    try {
      const response = await EventService.getEvents()
      commit('SET_EVENTS', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async fetchEvent({ commit, getters }, id) {
    const event = getters.getEventById(id)
    if (event) {
      commit('SET_EVENT', event)
    } else {
      try {
        const response = await EventService.getEvent(id)
        commit('SET_EVENT', response.data)
      } catch (e) {
        console.log(e)
      }
    }
  },
  async createEvent({ rootState, dispatch }, event) {
    let uploadedFile
    if (event.layout) {
      const formData = new FormData()
      formData.append('file', event.layout)
      try {
        uploadedFile = await MediaService.uploadFile(
          formData,
          rootState.auth.token
        )
      } catch (e) {
        console.log(e)
      }
    }

    try {
      const response = await EventService.createEvent(
        event,
        uploadedFile,
        rootState.auth.token
      )
      dispatch(
        'venues/addEventToVenue',
        {
          venueId: event.venue,
          eventId: response.data
        },
        { root: true }
      )
    } catch (e) {
      console.log(e)
    }
  },
  async updateEvent({ rootState }, { oldVersion, event }) {
    let uploadedFileId
    if (event.layout && event.layout.manuallyAdded) {
      uploadedFileId = event.layout.id
    } else if (event.layout.processing) {
      if (oldVersion.layout) {
        MediaService.deleteFile(oldVersion.layout.id, rootState.auth.token)
      }
      const formData = new FormData()
      formData.append('file', event.layout)
      const uploadedFile = await MediaService.uploadFile(
        formData,
        rootState.auth.token
      )
      uploadedFileId = uploadedFile.data.id
    } else {
      if (oldVersion.layout) {
        MediaService.deleteFile(oldVersion.layout.id, rootState.auth.token)
        uploadedFileId = ''
      }
    }
    const eventData = {
      title: event.title,
      school: event.school,
      date: event.date,
      ticket_price: event.ticket_price,
      venue: event.venue,
      codigo_del_evento: event.code,
      description: event.description,
      fbUrl: event.fbUrl,
      layout: uploadedFileId
    }
    try {
      await EventService.updateEvent(
        oldVersion.id,
        eventData,
        uploadedFileId,
        rootState.auth.token
      )
    } catch (e) {
      console.log(e)
    }
  },
  async deleteEvent({ dispatch, rootState, commit, state }, event) {
    if (event.venue) {
      await dispatch(
        'venues/deleteEventFromVenue',
        {
          venueId: event.venue.ID,
          eventId: event.id
        },
        { root: true }
      )
    }
    if (event.layout) {
      MediaService.deleteFile(event.layout.id, rootState.auth.token)
    }
    const events = state.events.filter(eventObj => {
      return eventObj.id !== event.id
    })
    await EventService.deleteEvent(event.id, rootState.auth.token)
    await dispatch('users/fetchGraduatesByEvent', event.id, {
      root: true
    })

    rootState.users.graduates.forEach(async graduate => {
      await dispatch('payments/fetchPaymentsById', graduate.id, { root: true })
      rootState.payments.payments.forEach(payment => {
        dispatch('payments/deletePayment', payment.id)
      })
      dispatch('users/deleteUser', graduate.id, { root: true })
    })
    commit('SET_EVENTS', events)
  }
}

export const getters = {
  getEventById: state => id => {
    return state.events.find(event => event.id === id)
  }
}
