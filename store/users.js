import UserService from '@/services/UserService'

export const state = () => ({
  graduates: [],
  graduate: {},
  agents: [],
  agent: {}
})

export const mutations = {
  SET_GRADUATES(state, graduates) {
    state.graduates = graduates
  },
  SET_GRADUATE(state, graduate) {
    state.graduate = graduate
  },
  SET_AGENTS(state, agents) {
    state.agents = agents
  },
  SET_AGENT(state, agent) {
    state.agent = agent
  }
}

export const actions = {
  async fetchGraduates({ commit, rootState }) {
    try {
      const response = await UserService.getGraduates(rootState.auth.token)
      commit('SET_GRADUATES', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async fetchGraduate({ commit, rootState, getters }, id) {
    const graduate = getters.getGraduateById(id)
    if (graduate) {
      commit('SET_GRADUATE', graduate)
    } else {
      try {
        const response = await UserService.getGraduate(id, rootState.auth.token)
        commit('SET_GRADUATE', response.data)
      } catch (e) {
        console.log(e)
      }
    }
  },
  async fetchGraduatesByEvent({ commit, rootState }, id) {
    try {
      const response = await UserService.getGraduatesByEventId(
        id,
        rootState.auth.token,
        1
      )
      if (response.headers['x-wp-totalpages'] > 1) {
        let graduates = response.data
        for (let i = 1; i < response.headers['x-wp-totalpages']; i++) {
          let newGraduates = await UserService.getGraduatesByEventId(
            id,
            rootState.auth.token,
            i + 1
          )
          //graduates.push(...newGraduates.data)
          await graduates.push(...newGraduates.data)
        }
        commit('SET_GRADUATES', graduates)
      } else {
        commit('SET_GRADUATES', response.data)
      }
    } catch (e) {
      console.log(e)
    }
  },
  async updateGraduate({ rootState }, { id, userData }) {
    try {
      await UserService.updateGraduate(
        id,
        {
          username: userData.username,
          first_name: userData.name,
          last_name: userData.lastname,
          email: userData.email,
          phone: userData.phone,
          tickets: userData.tickets,
          table: userData.table,
          special_menu: {
            wheelchair: userData.special_menu.wheelchair,
            kids: userData.special_menu.kids,
            allergies: userData.special_menu.allergies,
            diabetic: userData.special_menu.diabetic,
            vegetarian: userData.special_menu.vegetarian
          }
        },
        rootState.auth.token
      )
    } catch (e) {
      console.log(e)
    }
  },
  async fetchAgents({ commit, rootState }) {
    try {
      const response = await UserService.getAgents(rootState.auth.token)
      commit('SET_AGENTS', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async fetchAgent({ commit, rootState }, id) {
    try {
      const response = await UserService.getAgent(id, rootState.auth.token)
      commit('SET_AGENT', response.data)
    } catch (e) {
      console.log(e)
    }
  },
  async createAgent({ rootState }, user) {
    try {
      await UserService.createAgent(
        {
          username: user.username,
          email: user.email,
          first_name: user.firstname,
          last_name: user.lastname,
          phone: user.phone,
          password: user.password
        },
        rootState.auth.token
      )
    } catch (e) {
      console.log(e)
    }
  },
  async updateAgent({ rootState, dispatch }, { id, user }) {
    const userObj = {
      username: user.username,
      email: user.email,
      first_name: user.firstname,
      last_name: user.lastname,
      phone: user.phone
    }
    try {
      await UserService.updateAgent(id, userObj, rootState.auth.token)
      dispatch('fetchAgent', id)
    } catch (e) {
      console.log(e)
    }
  },
  async deleteUser({ rootState }, id) {
    await UserService.deleteUser(id, rootState.auth.token)
  },
  async sendConfirmationMail({ rootState }, id) {
    let response = await UserService.sendConfirmationEmail(
      id,
      rootState.auth.token
    )
    console.log(response)
  }
}

export const getters = {
  getGraduateById: state => id => {
    return state.graduates.find(graduate => graduate.id === id)
  }
}
