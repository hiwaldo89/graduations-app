export default function({ store, req }) {
  return store.dispatch('auth/initAuth', req)
}
